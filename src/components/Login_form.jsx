import { Form, Formik, Field } from "formik";

export function LoginForm({ onSubmit, innerRef }) {
  let inicial_data = {
    user_name: "",
    password: "",
  };
  let submit = (values, actions) => {
    actions.setSubmitting(true);
    onSubmit(values);
    actions.resetForm();
    actions.setSubmitting(false);
  };

  return (
    <Formik initialValues={inicial_data} onSubmit={submit} innerRef={innerRef}>
      {({ isSubmitting }) => {
        return (
          <Form>
            <div className="form-group py-3">
              <label htmlFor="username">Nombre de usuario</label>
              <Field
                className="form-control"
                type="text"
                name="user_name"
                placeholder="ingrese su nombre de usuario"
                id="user_name"
              />
            </div>
            <div className="form-group">
              <label htmlFor="user_name">Contraeña</label>
              <Field
                className="form-control"
                type="password"
                name="password"
                placeholder="ingrese su contraseña"
                id="password"
              />
            </div>
          </Form>
        );
      }}
    </Formik>
  );
}
