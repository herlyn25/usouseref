import { useRef } from "react";
import { LoginForm } from "../components/Login_form";

export function Login(){
    let ref=useRef(null)
    let login=(values)=>{
        console.log(values);
    }
    return (
      <div className="container py-5">
        <div className="row">
          <div className="col-6"></div>
          <div className="col-6">
            <LoginForm onSubmit={login} innerRef={ref}/>
            
              <button className="btn btn-primary" 
              onClick={()=>{
                  ref.current.submitForm();
              }}>
                Login
              </button>
            
          </div>
        </div>
      </div>
    );
}